<?php

  session_start ();
  $alphanum = 'ABCDEFGHIJKLMNPQRSTUVWXYZ123456789';
  $rand = substr (str_shuffle ($alphanum), 0, 5);
  $bgNum = rand (1, 4);
  $image = imagecreatefrompng ('captcha.png');
  $textColor = imagecolorallocate ($image, 0, 0, 0);
  imagestring ($image, 5, 28, 4, $rand, $textColor);
  
  $_SESSION['captcha'] = md5 ($rand);
  
  header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
  header ('Last-Modified: ' . gmdate ('D, d M Y H:i:s') . ' GMT');
  header ('Cache-Control: no-store, no-cache, must-revalidate');
  header ('Cache-Control: post-check=0, pre-check=0', false);
  header ('Pragma: no-cache');
  header ('Content-type: image/png');
  imagepng ($image);
  imagedestroy ($image);
?>